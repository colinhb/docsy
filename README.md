# Ivy Natal's Docsy fork

This Hugo theme is used for Ivy Natal's internal handbook. It is forked from Google's [Docsy](https://github.com/google/docsy) theme. The upstream README has been moved to [README-upstream.md](README-upstream.md).

## Changes

Here are the key changes from upstream:

- The upstream `docs` layouts are used for top-level content. (They're now `_default`.)
- The sidebar tree is rooted at `.Site.Home` instead of `.FirstSection`.
- The breadcrumb navigation is no longer skips the `.Site.Home` section.

In response to these changes, we've also:

- Removed the sections/types `blog` and `community`.
- Removed the userguide to avoid having it become stale.
